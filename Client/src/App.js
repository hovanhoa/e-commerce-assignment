import React from 'react';
import Mainpage from './Mainpage.js'
import SearchPage from './SearchPage.js'
import Product from './components/Product';
import PaymentPage from './PaymentPage.js';
import ProductsPage from './ProductsPage.js';
import ProfilePage from './ProfilePage.js';
import axios from 'axios';
import { Route, BrowserRouter as Router, Routes } from "react-router-dom"
const App = () =>{
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Mainpage/>}/>
                <Route path="/products" element={<ProductsPage/>}/>
                <Route path="/login" element={<SearchPage/>}/>
                <Route path="/payment" element={<PaymentPage/>}/>
                <Route path="/profile" element={<ProfilePage/>}/>  
            </Routes>
        </Router>
     );
     
}

export default App;

