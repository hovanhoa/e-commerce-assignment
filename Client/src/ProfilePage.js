import React from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Profile from './components/Profile/Profile';
const ProfilePage = () =>{
    return (
    <div className = "App">
        <Header/>
            <Profile />
        <Footer/>
    </div>
     );
     
}

export default ProfilePage;