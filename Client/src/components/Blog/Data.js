export const Data=[
    {title: "BENEFITS OF EATING HEALTHY ALL YEAR ROUND",img: 'image/blog-1.jpeg', Para: "Food benefits, Healthy food", Time:"February 20, 2021", User:"Fruit2U"},
    {title: "TOP FOODS WOMEN SHOULD CONSUME TO STAY HEALTHY",img: 'image/blog-2.jpeg', Para: "Healthy food", Time:"March 08, 2022", User:"Fruit2U"},
    {title: "A COMPLETE VEGAN GUIDE FOR BEGINNERS",img: 'image/blog-3.jpeg', Para: "Food trends, Vegan recipes", Time:"August 25, 2021", User:"Nature'sBasket"},
    {title: "HEALTHY SNACK OPTIONS TO MUNCH ON",img: 'image/blog-4.jpeg', Para: "Healthy food, Healthy snack", Time:"January 21, 2022", User:"Fruit2U"},
    {title: "SUPERFOODS FOR GREAT SKIN AND HAIR",img: 'image/blog-5.jpeg', Para: "Healthy food", Time:"February 01, 2022", User:"Fruit2U"}
]