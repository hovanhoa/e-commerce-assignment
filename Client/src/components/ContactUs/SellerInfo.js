import React from 'react'

function SellerInfo() {
    return (
        <div className="icons-container">

        <div className="icons">
            <i className="fas fa-map-marker-alt"></i>
            <h3>address</h3>
            <p>Dist. 10, Ho Chi Minh City, Viet Nam</p>
        </div>

        <div className="icons">
            <i className="fas fa-envelope"></i>
            <h3>email</h3>
            <p>fruit2u@gmail.com</p>
            {/* <p>anasbhai@gmail.com</p> */}
        </div>

        <div className="icons">
            <i className="fas fa-phone"></i>
            <h3>phone</h3>
            <p>(+84) 123 456 789</p>
            <p>(+84) 111 222 333</p>
        </div>

    </div>
    )
}

export default SellerInfo
