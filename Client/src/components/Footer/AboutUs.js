import React from 'react'

function AboutUs() {
    return (
        <div className="box">
        <h3>about us</h3>
        <p>Fruit2U is a friendly distribution system specializing in providing products made of organic and fresh fruits. We bring quality products for health to consumers.</p>
    </div>
    )
}

export default AboutUs
