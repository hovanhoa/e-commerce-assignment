import React from 'react'

function Brand() {
    return (
        <a href="#" className="logo"> <i className="fas fa-leaf"></i> Fresh2U </a>
    )
}

export default Brand
