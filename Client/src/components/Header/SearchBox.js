import React from 'react'
import {useState} from 'react'
import SearchModal from './SearchModal'

function SearchBox() {
    const [keyword, setKeyword] = useState('');
    const [trigger, setTrigger] = useState(false);

    function handleSubmit(event) {
        document.querySelector('.search-form').classList.toggle('active');
        event.preventDefault();
        if (event.target[0].value != '') {
            setTrigger(true);
        }
        setKeyword(event.target[0].value);
        document.querySelector('.search-form').reset();
    }

    return (
    <div>
        <form action="" className="search-form" onSubmit={handleSubmit}>
            <input type="search" name="" placeholder="search here..." id="search-box"></input>
            <label for="search-box" className="fas fa-search"></label>
        </form>
        {SearchModal(keyword, trigger, setTrigger)}
    </div>
    )
}

export default SearchBox
