import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import {Row, Col, Card} from 'react-bootstrap'
import {useState} from 'react'
import Popup from '../Product/Popup.js'

function SearchModal(keyword, trigger, setTrigger) {
    const closeHandler = () => {
      setTrigger(false)
    }

    const [Popple,setPopple] = useState(-1);
    const [b,setB] = useState(1);
    const [c,setC] = useState(0);

    function toggleFlag(index){
      let x = document.querySelectorAll(`.flag-${index}`)
      let a = 0;
      x.forEach(item=> {
          a ++;
          item.classList.toggle('active');
      })
    }

    function buyItem(props,index){
      if (!sessionStorage.getItem('email')){
          window.location = "/login";
          return;
      }
      var x = JSON.parse(sessionStorage.getItem('Order'))
      for(const a in x){
          let com = JSON.parse(x[a])
          if (com[1] === props[1]){
              com[4]++;
              x[a] = JSON.stringify(com)
              sessionStorage.setItem('Order',JSON.stringify(x));
              return;
        }
      }
      x.push(JSON.stringify({0:index,1:props[1],2:props[2],3:props[3],4:1}));
      sessionStorage.setItem('Order',JSON.stringify(x));  
    }

    const searchData = () => {
      let data = [];
        if (sessionStorage.getItem('Data')){
          data = (JSON.parse(sessionStorage.getItem('Data')));
        }
        data = data.map((data) => JSON.parse(data));
        console.log(data)
        return data.map((data, index) => String(data['1']).toLowerCase().includes(keyword.toLowerCase()) ? 
        (
        <Col>
          <Card className="text-center">
            <Card.Img variant="top" src={data[2]} style={{height: "18rem"}}/>
            <Card.Body>
              <Card.Title><h2>{data[1]}</h2></Card.Title>
              <Card.Text>
                Price: {parseInt(data[3]).toLocaleString()} VND
              </Card.Text>
              <Row className="m-auto">
                <Col xs={6}><Button variant="outline-primary" onClick={()=>buyItem(data,index)}><i class="fas fa-shopping-cart"></i></Button></Col>
                <Col xs={6}><Button variant="outline-success" onClick={()=>{setPopple(index); setB(data[2])}}><i class="fas fa-eye"></i></Button></Col>
              </Row>
            </Card.Body>
          </Card>
          {Popup(data,Popple,index,setPopple,b,setB,c,setC)}
        </Col>
        )
        :
        null
      )
    }

    return(
      trigger == true ?
      <Modal show={trigger} onHide={closeHandler} centered size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Search results for "{keyword}"</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row xs={1} md={2} lg={3} className="g-4">
            {searchData()}
            </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={closeHandler}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      :        
    (null)
  )
}

export default SearchModal
