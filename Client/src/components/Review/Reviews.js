import React from 'react'

function Reviews() {
    return (
        <div className="swiper review-slide">
    
        <div className="swiper-wrapper">

            <div className="swiper-slide slide">
                <p>Responsive and speedy delivery, fruit quality is good and fresh. This is my second order and will continue to purchase with gofruits.</p>
                <div className="user">
                    <img src="image/pic-1.png" alt=""/>
                    <div className="info">
                        <h3>john deo</h3>
                        <span>5 days ago</span>
                    </div>
                </div>
            </div>

            <div className="swiper-slide slide">
                <p>Price reasonable, convenience for everyone to buy online during covid19 - social distancing, fast delivery, fast reply on their messenger, still got discount voucher. Definitely will purchase again. Keep up the good work.</p>
                <div className="user">
                    <img src="image/pic-2.png" alt=""/>
                    <div className="info">
                        <h3>okkie lee</h3>
                        <span>a month ago</span>
                    </div>
                </div>
            </div>

            <div className="swiper-slide slide">
                <p>Good service, delicious fruit, worth to buy, worth to try.</p>
                <div className="user">
                    <img src="image/pic-3.png" alt=""/>
                    <div className="info">
                        <h3>jing tyng chen</h3>
                        <span>2 days ago</span>
                    </div>
                </div>
            </div>

            <div className="swiper-slide slide">
                <p>Fast and fresh vege & fruits. Well packed and clean which is most important . Thanks so much</p>
                <div className="user">
                    <img src="image/pic-4.png" alt=""/>
                    <div className="info">
                        <h3>harry jul</h3>
                        <span>5 days ago</span>
                    </div>
                </div>
            </div>

            <div className="swiper-slide slide">
                <p>Excellent service and very fresh fruits by them. My mum is currently in quarantine in one of the hotel provided, too bad the food provided is bad and no fruit. So I found go fruits and they can do next day delivery with fresh fruits and very reasonable price! Highly recommended</p>
                <div className="user">
                    <img src="image/pic-5.png" alt=""/>
                    <div className="info">
                        <h3>vivian chin</h3>
                        <span>2 months ago</span>
                    </div>
                </div>
            </div>

            <div className="swiper-slide slide">
                <p>Very fresh fruits and very responsive customer service . Courteous also. Highly recommended.</p>
                <div className="user">
                    <img src="image/pic-6.png" alt=""/>
                    <div className="info">
                        <h3>Mina Nguyen</h3>
                        <span>3 days ago</span>
                    </div>
                </div>
            </div>

        </div>

    </div>
    )
}

export default Reviews
